package chapter4;

public class Main {
    public static void main(String[] args) {

        // 조건문
        
        int num1 = 2;
        int num2 = 3;
        boolean flg1 = true;
        boolean flg2 = false;

        if(num1 < num2) {
            System.out.println("num2가 num1 보다 큽니다.");
        }

        if(flg1) {
            System.out.println("flg1은 TRUE입니다.");
        }

        if(!flg2) {
            System.out.println("flg1은 FALSE입니다.");
        }
        
        // 반복문

        // while문
        while(num1 < 5) {
            num1 = num1 + 1;
        }

        // do-while문
        do {
            num2 = num2 + 1;
        }while(num2 < 10);

        // for문
        for (int i = 0 ; i < 5 ; i++) {
            System.out.println(i+1);
        }

        // for-each문
        int[] nums = {1,2,3,4,5};
        for (int num : nums) {
            System.out.println("for-each문 출력 : "+num);
        }
    }
}
