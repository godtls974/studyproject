package chapter12;

import java.util.Arrays;

class Parent {
    private int a = 10;
    public int b = 20;
}

class Child extends Parent{

    public int c = 30;
    void display() {
        //System.out.println (a); //private 참조 불가능
        System.out.println (b);
        System.out.println (c);
    }
}

public class Main {
    public static void main(String[] args) {

        Child child1 = new Child();
        Child child2 = new Child();

        /*
         * toString
         */
        System.out.println("child1 : "+child1);
        System.out.println("child2 : "+child2);
        System.out.println("child1의 toString : "+child1.toString());
        System.out.println("child2의 toString : "+child2.toString());

        /*
         ㄴ* equals() 메소드
         */
        String str1 = "abcd";
        String str2 = new String("abcd");

        if (str1 == str2) {
            System.out.println("두개의 값은 일치합니다. (==)");
        } else {
            System.out.println("두개의 값은 일치하지 않습니다. (==)");
        }

        if (str1.equals(str2)) {
            System.out.println("두개의 값은 일치합니다. (equals)");
        } else {
            System.out.println("두개의 값은 일치하지 않습니다. (equals)");
        }

        /*
         * charAt() 메소드
         */
        String str = "가나다라마바사 아수라 발발타";
        System.out.println(str);

        for (int i = 0 ; i < str.length() ; i ++) {
            System.out.print("'" + str.charAt(i) + "'");
        }

        /*
         * compareTo() 메소드
         */

        String a = "abcd";

        System.out.println("원본 문자열 : " + a);
        System.out.println("abcf와 비교 " + a.compareTo("abcf"));
        System.out.println("abcd와 비교 " + a.compareTo("abcd"));
        System.out.println("Abcd와 비교 " + a.compareTo("Abcd"));
        System.out.println("Abcd와 비교 (대소문자 구분X) " + a.compareToIgnoreCase("Abcd"));

        /*
         * concat() 메소드
         */
        
        String concat1 = "JAVA";
        String concat2 = "";

        System.out.println(concat1.concat(" : 자바"));
        System.out.println(concat2.concat(" : 자바"));

        /*
         * indexOf() 메소드
         */

        String indexStr = "가나다라마바사";

        System.out.println("가의 index : " + indexStr.indexOf('가'));
        System.out.println("바의 index : " + indexStr.indexOf('바'));
        System.out.println("다의 index : " + indexStr.indexOf('다'));
        System.out.println("파의 index : " + indexStr.indexOf('파'));
        
        /*
         * trim() 메소드
         */

        String trimStr = new String(" Java     ");

        System.out.println("원본 문자열 : " + trimStr);
        System.out.println(trimStr + '|');
        System.out.println(trimStr.trim() + '|');
        System.out.println("trim() 메소드 호출 후 원본 문자열 : " + trimStr);

        /*
         * append() 메소드
         * concat() 메소드 보다 내부적인 처리 속도가 훨씬 빠르다 (상위호환)
         */

        StringBuffer appendStr = new StringBuffer("고");

        System.out.println(appendStr.append("양이"));


        /*
         * 박싱 / 언박싱
         */

        Integer num1 = new Integer(10);
        Integer num2 = new Integer(20);
        Integer num3 = new Integer(10);

        System.out.println(num1 < num2);       // true
        System.out.println(num1 == num3);      // false
        System.out.println(num1.equals(num3)); // true

        /*
         * binarySearch() 메소드
         */
        
        int [] binaryArr = new int[100];

        for (int i = 0 ; i < binaryArr.length ; i ++) {
            binaryArr[i] = i;
        }
        System.out.println("binarySearch 결과 : "+Arrays.binarySearch(binaryArr, 56));

        int [] testArr = {1,2,3,4,5,7,7};
        System.out.println("testArr 중복값 결과 : "+Arrays.binarySearch(testArr, 7));

        /*
         * sort() 메소드
         */

        
        int[] sortArr = {5,4,1,2,6,8};

        System.out.println("sort() 메소드 예제");

        System.out.println("기존 배열 : ");
        for (int num : sortArr) {
            System.out.print(num + " ");
        }

        System.out.println();
        Arrays.sort(sortArr);

        System.out.println("sort 적용 배열 : ");
        for (int num : sortArr) {
            System.out.print(num + " ");
        }
    }
}
