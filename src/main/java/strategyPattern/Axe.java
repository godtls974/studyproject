package strategyPattern;

public class Axe implements Weapon {
    
    @Override
    public void attack() {
        System.out.println("도끼로 공격");
    }
}
