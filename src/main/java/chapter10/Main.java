package chapter10;

class Parent {
    private int a = 10;
    public int b = 20;
}

class Child extends Parent{

    public int c = 30;
    void display() {
        //System.out.println (a); //private 참조 불가능
        System.out.println (b);
        System.out.println (c);
    }
}

public class Main {
    public static void main(String[] args) {
        Child child = new Child();

        child.display();
    }
}
