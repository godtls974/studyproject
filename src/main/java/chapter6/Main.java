package chapter6;

// 클래스
public class Main {
    // 필드 영역
    private String name;
    private int age;

    // 메소드 영역
    public void createMethod(){
        System.out.println("메소드가 선언 되었습니다.");
    }

    // 인스턴스화
    Instance cat = new Instance(); // cat은 Instance 클래스의 '인스턴스'
    Instance dog = new Instance(); // dog은 Instance 클래스의 '인스턴스'
}
