package chapter5;

import java.lang.reflect.Array;

public class Main {

    public static void main(String[] args) {

        // 1차원 배열
        int[] grade1 = {70,80,90};
        int[] grade2;
        grade2 = new int[] {20,30,40};

        for (int num : grade1) {
            System.out.println("grade1 : "+num);
        }

        for (int num : grade2) {
            System.out.println("grade2 : "+num);
        }

        // 2차원 배열
        int[][] socreArr = {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15}};

        int cnt = 1;
        for (int[] arr1 : socreArr) {
            System.out.println("------------array"+cnt);
            for (int num : arr1) {
                System.out.println("score : "+num);
            }
            cnt++;
        }
    }
}
