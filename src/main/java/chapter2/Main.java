package chapter2;

public class Main {

    public final String str1 = "파이널";
    public String str2 = "일반";

    public static void main(String[] args) {

        /*
        byte num1 = 100;        // OK
        byte num2 = 200;        // Type mismatch
        int num3 = 9876543210;  // Out of range
        long num4 = 9876543210; // Out of range
        float num5 = 3.14;      // Type mismatch
        float num6 = 3.14f;      // 컴퓨터는 소수점 데이터가 저장되는 경우 기본적으로 double형으로 인식하게됨
        */

        int bitNum1 = 1;    // 0000 0001
        int bitNum2 = 3;    // 0000 0011

        // 비트 연산자
        System.out.println(bitNum1 & bitNum2);    // 0000 0001: 01과 11을 비트 AND하면 01이 됨
        System.out.println(bitNum1 | bitNum2);    // 0000 0011: 01과 11을 비트 OR하면 11이 됨
        System.out.println(bitNum1 ^ bitNum2);    // 0000 0010: 01과 11을 비트 XOR하면 10이 됨
        System.out.println(~bitNum1);             // 0000 0010: 01을 NOT 부정 하므로, 1111 1101이 됨
    }

    public void test(String a, String b){
        //str1 = a;
        str2 = b;

    }
}
