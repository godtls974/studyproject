
--- 변수란 ---
데이터를 저장하기 위해 프로그램에 의해 이름을 할당받은 메모리 공간을 의미한다.

--- 변수의 종류 ---
1. 기본형 변수 : Stack 영역에 저장되며, 실제 연사에 사용되는 변수.
- 정수형 : byte, short, int, long
- 실수형 : float, double
- 문자형 : char
- 논리형 : boolean
ex) 일반적인 하드코딩 변수선언

2. 참조형 변수 : 주소값은 Stack 에 실제 값은 Heap 영역에 저장되며, 8개의 기본형 벼수를 사용하여 사용자가 직접 만들어 사용하는 변수를 의미 (주소값 참조)
ex) New 선언
※ 참고 링크 : https://colossus-java-practice.tistory.com/7
