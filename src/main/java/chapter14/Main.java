package chapter14;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        LinkedList<String> lnkList = new LinkedList<String>();

        // add() 메소드를 이용한 요소의 저장
        lnkList.add("넷");
        lnkList.add("둘");
        lnkList.add("셋");
        lnkList.add("하나");

        // for 문과 get() 메소드를 이용한 요소의 출력
        for (int i = 0; i < lnkList.size(); i++) {
            System.out.print(lnkList.get(i) + " ");
        }

        // remove() 메소드를 이용한 요소의 제거
        lnkList.remove(1);

        // Enhanced for 문과 get() 메소드를 이용한 요소의 출력
        for (String e : lnkList) {
            System.out.print(e + " ");
        }

        // set() 메소드를 이용한 요소의 변경
        lnkList.set(2, "둘");

        for (String e : lnkList) {
            System.out.print(e + " ");
        }


        // size() 메소드를 이용한 요소의 총 개수
        System.out.println("리스트의 크기 : " + lnkList.size());

        Stack<Integer> st = new Stack<Integer>(); // 스택의 생성

        // push() 메소드를 이용한 요소의 저장
        st.push(4);
        st.push(2);
        st.push(3);
        st.push(1);

        // peek() 메소드를 이용한 요소의 반환
        System.out.println(st.peek());
        System.out.println(st);

        // pop() 메소드를 이용한 요소의 반환 및 제거
        System.out.println(st.pop());
        System.out.println(st);

        // search() 메소드를 이용한 요소의 위치 검색
        System.out.println(st.search(4));
        System.out.println(st.search(3));


        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        System.out.println("List: ");
        Iterator<Integer> listIterator = list.iterator(); // Iterator 타입 변수 생성 및 초기화

        // Iterator 사용
        while (listIterator.hasNext()) {
            System.out.print(listIterator.next() + " ");
        }
        
        // For-each 사용
        for (int i : list) {
            System.out.print(i + " ");
        }
    }
}
