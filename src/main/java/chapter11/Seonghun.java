package chapter11;

import chapter11.FrontEndDeveloper;
import chapter11.bugFix;
import chapter11.useLanguage;

public class Seonghun extends FrontEndDeveloper implements bugFix, useLanguage {

    public Seonghun(int age, String language, String major, String name) {
        super(age, language, major, name);
    }

    @Override
    public void fixDesign() {
        System.out.println("디자인 수정했다. 퇴근하자");
    }

    @Override
    public void fixServer() {
        System.out.println("이건 백엔드 담당이야");
    }

    @Override
    public void useJava() {
        System.out.println("Java는 사용하지 않아, Changhyon에게 물어봐");
    }

    @Override
    public void usePython() {
        System.out.println("Python 사용하지 않아, Jaehak에게 물어봐");
    }

    @Override
    public void useJavaScript() {
        System.out.println("내가 사용하는 언어야");
    }
}
