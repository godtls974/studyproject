package chapter11;

public interface useLanguage {
    void useJava();
    void usePython();
    void useJavaScript();
}
