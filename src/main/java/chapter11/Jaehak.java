package chapter11;

import chapter11.BackEndDeveloper;
import chapter11.bugFix;
import chapter11.useLanguage;

public class Jaehak extends BackEndDeveloper implements bugFix, useLanguage {

    public Jaehak(int age, String language, String major, String name) {
        super(age, language, major, name);
    }

    @Override
    public void fixDesign() {
        System.out.println("이건 프론트 담당이야.");
    }

    @Override
    public void fixServer() {
        System.out.println("버그를 고쳤다. 퇴근하자");
    }

    @Override
    public void useJava() {
        System.out.println("Java는 사용하지 않아, Changhyon에게 물어봐");
    }

    @Override
    public void usePython() {
        System.out.println("내가 사용하는 언어야");
    }

    @Override
    public void useJavaScript() {
        System.out.println("JavaScript는 사용하지 않아, Seonghun에게 물어봐");
    }
}
