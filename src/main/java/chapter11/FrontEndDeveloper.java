package chapter11;

public abstract class FrontEndDeveloper extends Developer{

    public FrontEndDeveloper (int age, String language, String major, String name) {
        super(age, language, major, name);
    }

    @Override
    public void talk () {
        System.out.println(getAge()+"세의 백엔드 개발자 : "+ getName() +"은(는) 디자이너와 말을하고 있다");
        System.out.println("내 전공은 "+ getMajor() +"이고, "+ getLanguage()+"가 주언어야");
    }

    @Override
    public void meeting () {
        System.out.println("백엔드 개발자"+ getName() +"은(는) 야생의 기획자를 만났다!");
    }

    @Override
    public void emergency () {
        System.out.println("레이아웃이 개판이다. 오늘은 야근이다! 호우!");
    }
}
