package chapter11;

public abstract class Developer {

    private int age;
    private String language;
    private String major;
    private String name;

    public Developer(int age, String language, String major, String name) {
        this.age = age;
        this.language = language;
        this.major = major;
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return this.age;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    public String getLanguage() {
        return this.language;
    }

    public void setMajor(String major) {
        this.major = major;
    }
    public String getMajor() {
        return this.major;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

    public abstract void talk();
    public abstract void meeting();
    public abstract void emergency();
}
