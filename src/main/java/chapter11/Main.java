package chapter11;

/**
 * 추상클래스와 인터페이스의 공통점
 * - 둘다 추상 메소드를 지원한다.
 *
 * 추상클래스와 인터페이스의 차이점
 * - 추상클래스 = ~이다
 * - 인터페이스 = ~을 지원한다 (할 수 있다)
 * - 상속은 하나만 가능하므로 추상클래스를 상혹 후 나머지 공통 기능은 인터페이스로 구현
 */

public class Main {

    public static void main(String[] args){
        
        Changhyon changhyon = new Changhyon(30, "Java", "컴퓨터공학", "창현");

        changhyon.talk();
        changhyon.emergency();
        changhyon.fixDesign();
        changhyon.fixServer();
        changhyon.useJava();
        changhyon.usePython();
        changhyon.useJavaScript();

        System.out.println();
        System.out.println();

        Jaehak jaehak = new Jaehak(30, "Python", "소프트웨어 공학", "재학");

        jaehak.talk();
        jaehak.emergency();
        jaehak.fixDesign();
        jaehak.fixServer();
        jaehak.useJava();
        jaehak.usePython();
        jaehak.useJavaScript();

        System.out.println();
        System.out.println();

        Seonghun seonghun = new Seonghun(29, "Python", "컴퓨터공학", "성훈");

        seonghun.talk();
        seonghun.emergency();
        seonghun.fixDesign();
        seonghun.fixServer();
        seonghun.useJava();
        seonghun.usePython();
        seonghun.useJavaScript();
        
    }
}
