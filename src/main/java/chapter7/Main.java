package chapter7;

class Call {
    // 메소드 오버로딩1
    void callMethod(String a) {
        System.out.println("오버로딩 4");
    }
    // 메소드 오버로딩2
    void callMethod(String a, String b) {
        System.out.println("오버로딩 4");
    }
    // 메소드 오버로딩3
    void callMethod(int num) {
        System.out.println("오버로딩 4");
    }
    // 메소드 오버로딩4
    void callMethod(double dou) {
        System.out.println("오버로딩 4");
    }
}

// 클래스
public class Main {

    public static void main(String[] args) {

        Call call = new Call();

        // 오버로딩 호출
        call.callMethod("a");
        call.callMethod("a", "b");
        call.callMethod(1);
        call.callMethod(1.1);

        // this() 생성자 호출
        Car tcpCar = new Car(); System.out.println(tcpCar.getModel());

        callBack(1);
    }

    // 재귀 호출
    public static int callBack(int num) {
        if (num > 5) {
            System.out.println("재귀 호출 종료. : "+num);
            return num;
        }
        return callBack(num + 1);
    }
}
