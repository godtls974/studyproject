package adapterPattern;

import org.apache.logging.log4j.util.Strings;

public class Calculator implements Adapter{

    @Override
    public Float twice(Float f) {
        return (float) Math.twoTime(f.doubleValue());
    }

    @Override
    public Float half(Float f) {
        return (float) Math.half(f.doubleValue());
    }
}
