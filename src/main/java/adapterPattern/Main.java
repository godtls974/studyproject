package adapterPattern;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();

        System.out.println(calculator.twice(100f));
        System.out.println(calculator.half(50f));
    }
}
