package adapterPattern;

public interface Adapter {
    Float twice(Float f);
    Float half(Float f);
}
