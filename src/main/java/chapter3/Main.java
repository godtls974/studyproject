package chapter3;

public class Main {
    public static void main(String[] args) {


        int bitNum1 = 1;    // 0000 0001
        int bitNum2 = 3;    // 0000 0011

        // 비트 연산자
        System.out.println(bitNum1 & bitNum2);    // 0000 0001: 01과 11을 비트 AND하면 01이 됨
        System.out.println(bitNum1 | bitNum2);    // 0000 0011: 01과 11을 비트 OR하면 11이 됨
        System.out.println(bitNum1 ^ bitNum2);    // 0000 0010: 01과 11을 비트 XOR하면 10이 됨
        System.out.println(~bitNum1);             // 0000 0010: 01을 NOT 부정 하므로, 1111 1101이 됨
    }

}
